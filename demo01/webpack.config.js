let path = require('path')
module.exports = {
  entry: './main.js',
  output: {
    filename: 'bundle.js'
  },
  module: {
    rules:[
      {
        test: /\.js[x]?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015', 'react']
          }
        }
      }
      // ,
      // {
      //   test: /\.(txt)$/,
      //   use: [{
      //     loader: path.resolve('./loader'),
      //     options: {
      //       title: '花名册'
      //     }
      //   }]
      // }
    ]
  }
};
