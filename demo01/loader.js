let utils = require('loader-utils')
module.exports = function loader(source, map) {
    const options = utils.getOptions(this)
    let split1 = '|', split2 = ',';
    console.log(typeof options.type);
    if(options.type === '2') {
        split1 = ','
        split2 = '*'
    }
    const strList = source.split(split1)
    const list = strList.map(r => {
        const data = r.split(split2)
        return {
            name: data[0],
            age: data[1],
            sex: data[2]
        }
    }) 
    return `export const data = {title: '${options.title}', list: ${JSON.stringify(list)}}`
}